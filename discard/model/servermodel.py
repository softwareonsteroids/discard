import logging, random
from collections import deque
from discard.lib import (
    NormalCard,
    SpecialCard,
    ShapeColour,
    CardColour,
    Shapes,
    SpecialCardName,
    PlayingStatus,
    GameStatus
)
from discard.engine import PlayerState


class ServerModel(object):
    def __init__(self, players):
        self.logger = logging.getLogger(__name__)
        self.discard_deck, self._main_deck = [], []
        self._last_played, self._current_player = None, None
        self.game_state, self.state_history = {}, []
        self.colours = [
            ShapeColour.RED, ShapeColour.BLUE,
            ShapeColour.GREEN, ShapeColour.YELLOW
        ]
        self.shapes = [
            Shapes.CROSS, Shapes.SQUARE,
            Shapes.TRIANGLE, Shapes.CIRCLE,
            Shapes.CUP
        ]
        self.create_deck()
        self.create_states(players)
        self._players = players

    @property
    def main_deck(self):
        return self._main_deck

    @property
    def players(self):
        return self._players

    @property
    def current_player(self):
        return self._current_player

    @current_player.setter
    def current_player(self, player):
        self._current_player = player

    @property
    def last_played(self):
        return self._last_played

    @last_played.setter
    def last_played(self, player):
        self._last_played = player

    '''BEGIN: State methods'''
    def add_state(self, state):
        self.state_history.append(state)

    def get_player_state(self, player):
        return self.game_state[player]

    def set_player_state(self, player, state):
        self.game_state[player].player_state = state

    def get_last_state(self):
        if len(self.state_history) == 0:
            return None
        return self.state_history[-1]

    def create_states(self, players):
        for player in players:
            self.game_state[player] = PlayerState(PlayingStatus.PAUSED, [], [])
        self.logger.debug('Setting initial playing state for players')

    def set_win_status(self, player):
        self.game_state[player].set_win_status(GameStatus.WIN)
        losers = [
            self.players[index] for index in range(len(self.players))
            if index != self.players.index(player)
        ]
        for item in losers:
            self.game_state[item].set_win_status(GameStatus.LOSE)
        return player
    '''END: State methods'''

    '''BEGIN: Turn methods'''
    def find_player(self, cur_player):
        for player in self.players:
            if player == cur_player:
                self.logger.debug('Found player')
                return player

    def set_current_playing_to_player(self, player):
        self.current_player = self.find_player(player)
        if self.current_player:
            self.logger.debug(f'Current player: {self.current_player}')
            self.set_player_state(self.current_player, PlayingStatus.PLAYING)
            self.last_played = self.last_turn(player)
            if len(self.players) > 2:
                player_to_pause = self.last_turn(self.last_played)
                self.logger.debug(f'The state of the player to pause: {self.game_state[player_to_pause].player_state}')
                self.set_player_state(player_to_pause, PlayingStatus.PAUSED)
            else:
                self.set_player_state(self.last_played, PlayingStatus.PAUSED)
                if self.get_last_state() == 'SkipCardState':
                    self.set_player_state(self.last_played, PlayingStatus.PLAYING)
        return self.current_player

    def next_turn(self, player=None):
        self.logger.debug('Gettng next player to play')
        _player = None
        if player:
            _player = player
        else:
            _player = self.current_player
        index = (self.players.index(_player) + 1) % len(self.players)
        # This results in an infinite loop if skip card and pick card are combined
        while True:
            self.check_if_all_players_are_paused(_player)
            next_player = self.players[index]
            if self.get_player_state(next_player) == PlayingStatus.PAUSED:
                self.logger.info(f'Selected next player: {next_player}')
                return next_player
            index = (index + 1) % len(self.players)

    def last_turn(self, player=None):
        self.logger.debug('Getting player who played before this player')
        _player = None
        if player:
            _player = player
        else:
            _player = self.current_player
        index = (self.players.index(_player) - 1) % len(self.players)
        return self.players[index]

    def force_player_to_play(self, player):
        index = (self.players.index(player) + 1) % len(self.players)
        self.set_player_state(self.players[index], PlayingStatus.PAUSED)

    def check_if_all_players_are_paused(self, player):
        if any([self.get_last_state() == 'SkipCardState', len(self.players) == 2]):
            all_set_played = True
            for item in self.players:
                if self.get_player_state(item) == PlayingStatus.PAUSED:
                    all_set_played = False
                    break
            if all_set_played:
                self.force_player_to_play(player)
    '''END: Turn methods'''

    '''BEGIN: Card methods'''
    def _create_special_card_deck(self):
        deck = []
        for colour in self.colours:
            pick_one_card = SpecialCard(CardColour.WHITE, colour, SpecialCardName.PICKONE.value)
            deck.append(pick_one_card)
            pick_two_card = SpecialCard(CardColour.WHITE, colour, SpecialCardName.PICKTWO.value)
            deck.append(pick_two_card)
            question_card = SpecialCard(CardColour.WHITE, colour, SpecialCardName.QUESTION.value)
            deck.append(question_card)
            skip_card = SpecialCard(CardColour.WHITE, colour, SpecialCardName.SKIP.value)
            deck.append(skip_card)
            drop_card = SpecialCard(CardColour.WHITE, colour, SpecialCardName.DROP.value)
            deck.append(drop_card)
        return deck

    def _create_normal_card_deck(self):
        return [
            [
                NormalCard(CardColour.BLACK, colour, shape) for colour in
                self.colours for shape in self.shapes
            ] for i in range(0, 5)
        ]

    def create_deck(self):
        normal_deck = [card for deck in self._create_normal_card_deck() for card in deck]
        special_deck = self._create_special_card_deck()
        self.main_deck.append(normal_deck)
        self.main_deck.append(special_deck)
        self.main_deck[:] = [card for deck in self.main_deck for card in deck]
        self.logger.debug('Created deck')

    def get_colour(self, colour):
        names = [item.name.lower() for item in self.colours]
        return [
            item for name, item in zip(names, self.colours)
            if colour.lower() == name
        ]

    def get_shape(self, shape):
        names = [item.name.lower() for item in self.shapes]
        return [
            item for name, item in zip(names, self.shapes)
            if shape.lower() == name
        ]

    def does_shape_exist(self, shape):
        return shape.lower() in [item.name.lower() for item in self.shapes]

    def does_colour_exist(self, colour):
        return colour.lower() in [item.name.lower() for item in self.colours]

    def add_card(self, card):
        self.discard_deck.append(card)

    def cards_for_player(self, player):
        found = self.game_state[player]
        return found.current_deck

    def give_player_a_card(self, player, index):
        found = self.game_state[player]
        found.current_deck.append(self.get_a_card(index))
        self.game_state[player] = found

    def get_a_card(self, index):
        # Shuffle if main deck is small
        if len(self.main_deck) == 1:
            temp_list = deque(self.discard_deck)
            temp_deck = [
                temp_list.popleft() for index in range(0, len(temp_list) - 1)
            ]
            random.shuffle(temp_deck)
            self.main_deck.extend(temp_deck)
            self.discard_deck = list(temp_list)
        if index:
            return self.main_deck.pop(index)
        else:
            return self.main_deck.pop()

    def get_top_card(self):
        return self.discard_deck[-1]
    '''END: Card methods'''
