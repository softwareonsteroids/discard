 
import zmq
import sys
import logging
import os

from pathlib import Path
from uuid import uuid4
from discard.lib import (
    GameStatus,
    LoopChoices,
    MessageDestination,
    Request,
    Response,
    Stages,
    Config
)
from discard.model import (
    PlayerModel,
    Human
)
from discard.frontend.netclient import NetClient


def get_random_port():
    import socket
    s = socket.socket()
    s.bind(("", 0))
    port_num = s.getsockname()[1]
    s.close()
    return port_num


def enable_client_logging():
    rootlogger = logging.getLogger()
    rootlogger.setLevel(logging.DEBUG)

    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(logging.Formatter(logging.BASIC_FORMAT))

    rootlogger.addHandler(stream_handler)


class CmdUI(object):
    def __init__(self, port):
        self.logger = logging.getLogger(__name__)
        self._set_file_logger()
        self.ctx = zmq.Context()
        self.socket = self.ctx.socket(zmq.PAIR)
        self.socket.connect('tcp://127.0.0.1:{0}'.format(port))
        self.model = PlayerModel()
        self.player, self.current_player = None, None
        self.current_roomates = []
        self.stages = [Stages.LANDING]  # this shows which stage the client is in from LANDING till GAME_LOOP
        self.user_actions = dict()  # this shows the history of commands sent to the backend
        self.msg_recv = None
        self.choice = None      # this choice is used as a switch case to move in between stages

    def _set_file_logger(self):
        path = str(Path.cwd().joinpath('logs', f'cmdui_{str(uuid4())}.log'))
        file_handler = logging.handlers.RotatingFileHandler(path, maxBytes=(1048576 * 5),
                                                            backupCount=7)
        file_handler.setLevel(logging.DEBUG)
        f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(f_format)
        self.logger.addHandler(file_handler)

    '''BEGIN: User Input methods'''
    def get_str_input(self, question):
        while True:
            try:
                choice = input(question)
                if any((choice is None, not choice.strip())):
                    print('Error: Empty string entered!!!')
                    self.logger.error('Error: Empty string entered!!!')
                else:
                    return choice
            except Exception:
                raise

    def get_int_input(self, question):
        while True:
            try:
                choice = self.get_str_input(question)
                choice = int(choice)
                return choice
            except ValueError as err:
                print(err)
            except Exception:
                raise

    def validate_user_entry(self, input_func_cb,
                            input_question, validation_params):
        choice = input_func_cb(input_question)
        while (choice in validation_params) is False:
            print(f'Error: Option choosen not in valid options={validation_params}')
            self.logger.debug(f'Error: Option choosen not in valid options={validation_params}')
            choice = input_func_cb(input_question)
        return choice
    '''END: User Input methods'''

    ''' BEGIN: Menu string'''
    @staticmethod
    def landing_page_menu_str():
        return '\n'.join([
            '\n Choose from these options:',
            'j) Join an existing room(Recommended)',
            'c) Create a room',
            'q) Exit',
            '\nSelect option: '
        ])

    @staticmethod
    def room_menu_str():
        return 'How many players do you want to play with' + \
               '[1-7]: '

    @staticmethod
    def choose_room_menu_str():
        return 'Choose room to join: '

    @staticmethod
    def game_menu_str():
        return '\n'.join([
            '\nMenu Options:\n',
            'Select an option below related to an action.\n',
            'h -- help and rules in a browser',
            'r -- help and rules in your command line',
            'p -- play your turn. Note you have to play to skip',
            'q -- quit game\n',
            'Enter your option: '
        ])

    def choose_player_menu_str(self, roomates):
        return '\n'.join([
            '\nList of roomates:',
            roomates,
            'Choose initial player: '
        ])
    ''' END: Menu string '''

    ''' BEGIN: Helper methods '''
    def _send_message(self, msg):
        self.socket.send_pyobj(msg)
        self.logger.debug(f'Sent a {str(msg.cmd)} to tbe backend')

    def _create_new_user(self):
        print('\n\nEnter your credentials:\n')
        user_id = uuid4().hex
        user_name = self.get_str_input('What is your username: ')
        self.player = Human(user_id, self, self.model)
        self.player.nickname = user_name
        self.logger.debug(f'Created a new user: {self.player}')

    def _add_stage(self, stage):
        self.stages.clear()
        self.choice = None
        self.stages.append(stage)

    def _set_cur_player(self, msg):
        self.current_player = msg.get_payload_value('user_id')
        self.logger.debug(f'Current player={self.current_player}' \
                           f' , current roomates={str(self.current_roomates)}')
        cur_player = [item.get('nickname') for item in self.current_roomates
                      if self.current_player == item.get('user_id')]
        print('Currently playing: {0}'.format(cur_player[0]))
        self.logger.debug('Currently playing: {0}'.format(cur_player[0]))

    def _show_current_user_actions(self, action):
        self.user_actions[action] = True
        self.logger.debug(f'Current user actions = {str(self.user_actions)}')

    ''' BEGIN: Helper methods '''

    ''' BEGIN: Room methods '''
    def find_room(self):
        print('\n\n\t====Getting rooms to join====')
        if not self.user_actions.get('GET_ROOMS'):
            self._send_message(dict(
                cmd=Request.GET_ROOMS.name,
                dest=MessageDestination.WEB,
                req_type='GET'
            ))
            self._show_current_user_actions('GET_ROOMS')

    def create_room(self):
        if not self.user_actions.get('CREATE_A_ROOM'):
            print('\n\n\t====Creating a new room====')
            self._create_new_user()
            room_name = self.get_str_input("What is the room's name: ")
            num_of_players = self.validate_user_entry(
                input_func_cb=self.get_int_input,
                input_question=CmdUI.room_menu_str(),
                validation_params=[x for x in range(1, 8)]
            )
            num_of_players = num_of_players + 1
            self._send_message(dict(
                user_name=self.player.nickname,
                user_id=self.player.user_id,
                num_of_players=num_of_players,
                room_name=room_name,
                cmd=Request.CREATE_A_ROOM,
                dest=MessageDestination.WEB,
                req_type='POST'
            ))
            self._show_current_user_actions('CREATE_A_ROOM')

    def get_room(self, msg):
        room_list = msg.get_payload_value('rooms')
        if room_list:
            ls_ = [{key: value
                    for key, value in value.items()
                    if key != 'room_id'
                    } for value in room_list]
            ls_[:] = [str(ind) + ') ' + repr(value)
                      for ind, value in enumerate(ls_)
                      ]
            rooms_str = '\n'.join(ls_)
            print('The rooms available: ', rooms_str)
            choice = self.validate_user_entry(
                input_func_cb=self.get_int_input,
                input_question=CmdUI.choose_room_menu_str(),
                validation_params=[x for x in range(len(ls_))]
            )
            room = room_list[choice]
            self.logger.debug(f'Found room: {room}')
            return room
        print("Can't find rooms. You can try to find rooms",
              " again (Recommended) or create a room")
        self.logger.debug('Could not find a room')
        return None

    def join_room(self, room):
        if not self.user_actions.get('JOIN_ROOM'):
            print('\n\n\t=====Joining room====')
            self._create_new_user()
            self.player.room_id = room.get('room_id')
            self._send_message(dict(
                user_name=self.player.nickname,
                user_id=self.player.user_id,
                room_id=room.get('room_id'),
                cmd=Request.JOIN_ROOM,
                dest=MessageDestination.WEB,
                req_type='POST'
            ))
            self._show_current_user_actions('JOIN_ROOM')

    def get_roomates(self):
        if not self.user_actions['GET_ROOMATES']:
            self._send_message(dict(
                cmd=Request.GET_ROOMMATES.name,
                user_id=self.player.user_id,
                room_id=self.player.room_id,
                req_type='GET',
                dest=MessageDestination.WEB
            ))
            self._show_current_user_actions('GET_ROOMATES')
    ''' END: Room methods '''

    def create_new_game_conn(self):
        if not self.user_actions.get('START_GAME'):
            self._send_message(dict(
                cmd=Request.START_GAME.name,
                room_id=self.player.room_id,
                user_id=self.player.user_id,
                user_name=self.player.nickname,
                dest=MessageDestination.GAME
            ))
            self._show_current_user_actions('START_GAME')

    def choose_initial_player(self):
        if not self.user_actions.get('SET_INITIAL_PLAYER'):
            roomates = self.current_roomates[:]
            roomates.append(dict(nickname=self.player.nickname, user_id=self.player.user_id))
            ls = [str(ind) + ') ' + str(value)
                  for ind, value in enumerate(roomates)]
            roomates_str = '\n'.join(ls)
            choice = self.validate_user_entry(
                input_func_cb=self.get_int_input,
                input_question=self.choose_player_menu_str(roomates_str),
                validation_params=[x for x in range(len(roomates))]
            )
            roomate = roomates[choice]
            print(f'Chose roomate: {str(roomate)}')
            self._send_message(dict(
                cmd=Request.GAME_REQUEST,
                next_cmd=Request.SET_INITIAL_PLAYER,
                room_id=self.player.room_id,
                user_id=roomate.get('user_id'),
                user_name=roomate.get('nickname'),
                game_id=self.player.game_id,
                dest=MessageDestination.GAME,
                delivery=MessageDestination.UNICAST
            ))
            self._show_current_user_actions('SET_INITIAL_PLAYER,')

    ''' BEGIN: Menus '''
    def landing_page_menu(self):
        if not self.choice:
            choice = self.validate_user_entry(
                input_func_cb=self.get_str_input,
                input_question=CmdUI.landing_page_menu_str(),
                validation_params=['j', 'c', 'q']
            )
            self.choice = LoopChoices(choice)
        if self.choice == LoopChoices.JOIN_ROOM:
            self.find_room()
        if self.choice == LoopChoices.CREATE_ROOM:
            self.create_room()
        if self.choice == LoopChoices.LEAVE_GAME:
            self.close_game()

    def loop_menu(self):
        choice = self.validate_user_entry(
            input_func_cb=self.get_str_input,
            input_question=CmdUI.game_menu_str(),
            validation_params=['h', 'r', 'p', 'q']
        )
        answer = LoopChoices(choice)
        if answer == LoopChoices.PRETTY_HELP:
            return None
        elif answer == LoopChoices.CMD_RULES:
            return None
        elif answer == LoopChoices.PLAY_ROUND:
            return dict(
                dest=MessageDestination.GAME,
                **self.player.play()
            )
        elif answer == LoopChoices.LEAVE_GAME:
            self.current_player = self.player.user_id
            return dict(cmd=Request.STOP_GAME)
    ''' END: Menus '''

    ''' BEGIN: Message Handling '''
    def compose_message(self, msg):
        if self.stages[0] == Stages.LANDING:
            self.landing_page_menu()
        if self.stages[0] == Stages.CONNECTION_MESSAGE_SENT:
            self.create_new_game_conn()
        if self.stages[0] == Stages.CONNECTION_CREATED:
            self.get_roomates()
        if self.stages[0] == Stages.CHOSEN_INITIAL_PLAYER:
            self.choose_initial_player()
        if self.stages[0] == Stages.GAME_LOOP:
            self.game_loop(msg)

    def open_message(self, msg):
        if self.stages[0] == Stages.LANDING:
            if self.choice and self.choice == LoopChoices.JOIN_ROOM:
                room = self.get_room(msg)
                if room:
                    self.join_room(room)
                    self._add_stage(Stages.ROOM_RECEIVED)
                else:
                    del self.user_actions['GET_ROOMS']
            if self.choice and self.choice == LoopChoices.CREATE_ROOM:
                self.player.room_id = msg.get_payload_value('room_id')
                self.logger.debug(f'Player created and added themselves to a room with id: {self.player.room_id}')
                self._add_stage(Stages.CONNECTION_MESSAGE_SENT)
        elif self.stages[0] == Stages.ROOM_RECEIVED:
            print(msg.get_payload_value('prompt'))
            self.logger.debug(f"Joined a room: {self.player.room_id}")
            self._add_stage(Stages.CONNECTION_MESSAGE_SENT)
        elif self.stages[0] == Stages.CONNECTION_MESSAGE_SENT:
            # Waiting for the game to be set up
            print(
                " ".join(msg.get_payload_value('prompt').name.split('_'))
            )
            self.logger.debug(f"Received message from the server {str(msg.get_payload_value('prompt'))}")
            if msg.get_payload_value('prompt') == Response.GAME_HAS_STARTED:
                self.player.game_id = msg.get_payload_value('game_id')
                self.player.set_deck(msg.get_payload_value('cards'))
                self.logger.debug(f"Cards {str(msg.get_payload_value('cards'))}")
                print("Cards Received: ", msg.get_payload_value('cards'))
                self.player.top_card = msg.get_payload_value('extra_data')
                self._add_stage(Stages.CONNECTION_CREATED)
        elif self.stages[0] == Stages.CONNECTION_CREATED:
            # Making sure we do not handle stray messages
            if not msg.get_payload_value('prompt') == Response.GAME_HAS_STARTED:
                self.current_roomates = msg.get_payload_value('roomates')
                self.logger.debug(f'Received current roomates: {str(self.current_roomates)}')
                self._add_stage(Stages.CHOSEN_INITIAL_PLAYER)
        elif self.stages[0] == Stages.CHOSEN_INITIAL_PLAYER:
            print(msg.get_payload_value('prompt'))
            self.logger.debug(f"Received message from the server: {str(msg.get_payload_value('prompt'))}")
            self._add_stage(Stages.GAME_LOOP)
        elif self.stages[0] == Stages.GAME_LOOP:
            print(msg.get_payload_value('prompt'))
            self.logger.debug(f"Received message from the server: {str(msg.get_payload_value('prompt'))}")

    ''' END: Message Handling '''

    def close_game(self):
        self.logger.debug(f'Closing due to application panic')
        try:
            self.socket.send_pyobj(
                dict(cmd=GameStatus.ENDED),
                flags=zmq.NOBLOCK
            )  # using send in nonblocking raised zmq.EAGAIN
        except zmq.ZMQError as exc:
            if exc.errno == zmq.EAGAIN:
                pass
        self.socket.close()
        # self.ctx.term()
        self.logger.debug(f'Closing game')
        sys.exit(0)

    def game_loop(self, msg):
        if msg:
            self._set_cur_player(msg)
        msg_snd = self.loop_menu()
        while not msg_snd:
            msg_snd = self.loop_menu()
        if self.current_player == self.player.user_id:
            self._send_message(msg_snd)
        else:
            print('Not your turn!!',
                  self.current_player, ' is still playing!!')
            self._logger.debug(f'Not your turn!!!' \
                               f'{self.current_player} is still playing!!')

    def start(self):
        self.logger.info('CmdUI')
        msg_rcv = None
        try:
            while True:
                self.compose_message(msg_rcv)
                try:
                    # msg_snd = self.get_str_input('Enter a message: ')
                    # self.socket.send_pyobj(dict(cmd=msg_snd))
                    # Try to receive message
                    msg_rcv = self.socket.recv_pyobj(flags=zmq.NOBLOCK)
                except zmq.ZMQError as exc:
                    if exc.errno == zmq.EAGAIN:
                        pass
                    else:
                        raise

                self.logger.info(f'Received {str(msg_rcv)}')
                if msg_rcv and msg_rcv.get('cmd') == GameStatus.ENDED:
                    print('Player ended game session')
                    self.logger.debug('Player ended game session')
                    self.close_game()

                if msg_rcv:
                    self.open_message(msg_rcv)
        except KeyboardInterrupt:
            self.close_game()
        except Exception:
            self.close_game()


def main():
    enable_client_logging()
    ui, netclient = None, None
    try:
        port = get_random_port()
        netclient = NetClient(
            port,
            f'ws://localhost:{Config.MAINSERVER_PORT}/{Config.NETCLIENT_GAMESERVER_URL}'
            f'http://localhost:{Config.MAINSERVER_PORT}/{Config.NETCLIENT_MAINSERVER_URL}'
        )
        ui = CmdUI(port)
        netclient.start()
        ui.start()
    except SystemExit:
        # netclient.join()
        print('Application ended')
        ui.logger.debug('Application ended')
        os._exit(os.EX_OK)


if __name__ == '__main__':
    main()