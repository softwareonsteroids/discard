import zmq
import logging
import asyncio
import os
import signal

from functools import wraps
from threading import Thread
from zmq.eventloop.zmqstream import ZMQStream
from tornado import ioloop, websocket, httpclient, httputil, gen
from discard.lib import (
    GameStatus,
    MessageDestination,
    Message,
    Response
)

from tornado.platform.asyncio import AnyThreadEventLoopPolicy
asyncio.set_event_loop_policy(AnyThreadEventLoopPolicy())


class ServerDisconnectedError(Exception):
    """Exception triggered if the server closes the websocket connection arbitrarily"""
    pass


def retry(exceptions, tries=4, delay=3, backoff=3, logger=None):
    """
    Adapted from https://www.saltycrane.com/blog/2009/11/trying-out-retry-decorator-python/
    Retry calling the decorated function using an exponential backoff
    :param exceptions: The exception to check. may be a tuple of
            exceptions to check
    :param tries: Number of times to try (not retry) before giving up.
    :param delay: Initial delay between retries in seconds.
    :param backoff: Backoff multiplier (e.g. value of 2 will double the delay
            each retry)
    :param logger: Logger to use. If None, print.
    :return:
    """
    def decorator(f):
        @wraps(f)
        async def wrapped(*args, **kwargs):
            mtries, mdelay = tries, delay
            while mtries > 1:
                try:
                    return await f(*args, **kwargs)
                except exceptions as e:
                    warning = f'{e}, Retrying in {mdelay} seconds'
                    if logger:
                        logger.warning(warning)
                    else:
                        print(warning)
                    await gen.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return await f(*args, **kwargs)
        return wrapped  # true decorator
    return decorator


class NetClient(Thread):
    def __init__(self, port, server_url, room_url, **kwargs):
        super(NetClient, self).__init__()
        self.logger = logging.getLogger(__name__)
        self.wsconn = None
        self.wsconn_close = False
        self.wsconn_already_closed = False
        self.has_wsconn_initialised = False
        self.is_retrying_connection = False
        self.has_started_retry_connection = False
        self.forced_exit = False
        self.daemon = True
        self.game_url = server_url
        self.room_url = room_url
        self.ctx = zmq.Context.instance()
        self.socket = self.ctx.socket(zmq.PAIR)
        # do not use localhost, because it would complain
        # with error [zmq.error.ZMQError: No such device]
        # self.socket.bind('inproc://uisocket')
        self.socket.bind('tcp://127.0.0.1:{}'.format(port))
        self.stream = ZMQStream(self.socket)
        self.stream.on_recv(self.communicate_with_server)
        self.logger.debug(f'Running on a port: {str(port)}')
        self.user_id, self.user_name, self.room_id = None, None, None
        self.ioloop = ioloop.IOLoop()

    def set_conn_parameters(self, msg):
        if 'user_name' in msg.keys():
            self.user_name = msg.get('user_name')
        if 'room_id' in msg.keys():
            self.room_id = msg.get('room_id')
        if 'user_id' in msg.keys():
            self.user_id = msg.get('user_id')

    def compose_message(self, msg):
        self.set_conn_parameters(msg)
        msg_snd = Message(**msg)
        return Message.to_json(msg_snd)

    async def send_msg_to_web(self, msg):
        request = None
        msg_snd = {k: v for k, v in msg.items() if k not in ['req_type', 'dest']}
        self.logger.debug(f'Sending msg={str(msg_snd)} to handler={self.room_url}')
        if msg.get('req_type') == 'POST':
            msg_ = self.compose_message(msg_snd)
            request = httpclient.HTTPRequest(
                url=self.room_url,
                method=msg.get('req_type'),
                headers={
                    'Content-type': 'application/json',
                    'Accept': 'text/plain'
                },
                body=msg_
            )
        if msg.get('req_type') == 'GET':
            url = httputil.url_concat(self.room_url, msg_snd)
            request = httpclient.HTTPRequest(
                url=url
            )
        response = await httpclient.AsyncHTTPClient.fetch(request)
        res = response.body.decode()
        self.logger.debug(f'Received response={str(res)} from handler={self.room_url}')
        self.socket.send_pyobj(Message.to_obj(res))

    async def read_msg_from_websocket(self):
        if self.wsconn_close:
            if not self.wsconn_already_closed:
                self.wsconn.close()
                self.socket.send_pyobj(dict(
                    cmd=GameStatus.ENDED
                ))
                self.wsconn_already_closed = True
            return
        msg_recv = await self.wsconn.read_message()
        self.logger.debug(f'Received from websocket={msg_recv}')
        if msg_recv is None:
            self.has_wsconn_initialised = False
            self.is_retrying_connection = True
            return
        msg = Message.to_obj(msg_recv)
        self.logger.debug('Received from websocket(Decoded)={}'.format(str(msg)))
        if msg.cmd == Response.GAME_PONG:
            return None
        return msg

    async def send_msg_to_websocket(self, msg):
        if msg:
            try:
                msg_snd = self.compose_message(msg)
                if isinstance(self.wsconn,
                              websocket.WebSocketClientConnection):
                    self.logger.debug('Sending game request')
                    # Cannot send a dict(somehow)
                    await self.wsconn.write_message(msg_snd)
            except Exception:
                self.has_wsconn_initialised = False
                self.is_retrying_connection = True
        else:
            self.wsconn_close = True

    @retry(exceptions=ServerDisconnectedError)
    async def reconnect_ws(self):
        if not self.wsconn_already_closed:
            self.logger.debug('Recreating game server connection')
            try:
                url = httputil.url_concat(self.game_url, dict(
                    user_id=self.user_id,
                    room_id=self.room_id,
                    user_name=self.user_name
                ))
                self.wsconn = await websocket.websocket_connect(url)
            except Exception as err:
                raise ServerDisconnectedError('Server disconnected. Reconnecting...') from err
            else:
                if self.wsconn:
                    self.logger.debug('Connection established')
                    self.has_wsconn_initialised = True
                    self.is_retrying_connection = False

    async def init_game_conn(self, msg):
        self.logger.debug('Creating initial game server connection')
        try:
            self.set_conn_parameters(msg)
            url = httputil.url_concat(self.game_url, dict(
                user_id=self.user_id,
                room_id=self.room_id,
                user_name=self.user_name
            ))
            self.logger.debug(f'Connecting to url={url}')
            self.wsconn = await websocket.websocket_connect(url)
        except Exception as err:
            raise ServerDisconnectedError('Server was never started. Start server instance.') from err
        else:
            self.logger.debug('Connection established')
            # if self.wsconn and not self.has_wsconn_initialised:
            #     self.has_wsconn_initialised = True
            #     self.socket.send_pyobj(dict(cmd=GameStatus.STARTED, msg='Connection established'))
            if self.wsconn and not self.has_wsconn_initialised:
                self.has_wsconn_initialised = True

    async def communicate_with_server(self):
        try:
            while True:
                msg_rcv = self.socket.recv_pyobj()
                self.logger.debug(f'Received from playerui={str(msg_rcv)}')
                if self.is_retrying_connection and not self.has_started_retry_connection:
                    self.has_started_retry_connection = True
                    await self.reconnect_ws()

                if not self.is_retrying_connection:
                    if msg_rcv.get('cmd') == GameStatus.ENDED and not self.forced_exit:
                        self.cleanup()
                        break
                    elif msg_rcv.get('dest') == MessageDestination.WEB:
                        await self.send_msg_to_web(msg_rcv)
                    elif msg_rcv.get('dest') == MessageDestination.GAME:
                        if not self.has_wsconn_initialised:
                            await self.init_game_conn(msg_rcv)
                        await self.send_msg_to_websocket(msg_rcv)
                        msg_rcv = await self.read_msg_from_websocket()
                        self.socket.send_pyobj(msg_rcv)
        except ServerDisconnectedError:
            self.logger.error('Server connection can not be restored in time.')
            if not self.forced_exit:
                self.cleanup()
                os.kill(os.getpid(), signal.SIGINT)  # signal is only caught in the main thread

    def cleanup_on_panic(self):
        self.socket.send_pyobj(dict(cmd=GameStatus.ENDED))
        _ = self.socket.recv_pyobj()
        self.cleanup()

    def cleanup(self):
        self.forced_exit = True
        self.socket.close()
        self.ctx.term()
        self.logger.debug('Closing the netclient')
        self.ioloop.current().stop()

    def run(self):
        try:
            self.logger.debug(f'Running on ioloop = ${str(self.ioloop)}')
            self.ioloop.current().spawn_callback(self.communicate_with_server)
            self.ioloop.current().start()
        except KeyboardInterrupt:
            if not self.forced_exit:
                self.cleanup_on_panic()
