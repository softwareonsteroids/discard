import logging,random
from discard.model import ServerModel
from discard.engine.gameengine import GameEngine
from discard.lib import NumberOfCardsToDeal


class Controller(object):
    def __init__(self, players):
        self.logger = logging.getLogger(__name__)
        self.model = ServerModel(players)
        self.engine = GameEngine(self)
        self.has_initial_player = False
        self.init_cards()

    def init_cards(self):
        self.deal()
        while self.engine.is_card_special(self.model.get_top_card()):
            self.deal()

    @property
    def current_player(self):
        return self.model.current_player

    def set_initial_player(self, player):
        _player = None
        if not self.has_initial_player:
            self.logger.debug(f'Setting initial player to {player}')
            self.model.set_current_playing_to_player(player)
            self.has_initial_player = True
        else:
            _player = self.current_player
            self.logger.debug(f'Returning already set initial player: {_player}')
        return _player

    def deal(self):
        self.logger.debug('Dealing cards')
        num_to_deal = 0
        if len(self.model.players) == 2:
            num_to_deal = NumberOfCardsToDeal.TWO_PLAYER.value
        elif len(self.model.players) == 3:
            num_to_deal = NumberOfCardsToDeal.THREE_PLAYER.value
        else:
            num_to_deal = NumberOfCardsToDeal.OTHER_PLAYER.value
        temp_list = self.model.main_deck
        random.shuffle(temp_list)
        self.model.main_deck[:] = temp_list
        for index in range(0, num_to_deal):
            for player in self.model.players:
                self.model.give_player_a_card(player, index=None)
        for index, card in enumerate(reversed(self.model.main_deck)):
            if self.engine.is_card_normal(card):
                self.model.discard_deck.append(self.model.get_a_card(index))
                break

    def cards_for_player(self, player):
        return self.model.cards_for_player(player)

    @property
    def top_card(self):
        return self.model.get_top_card()

    def start_game(self):
        pass

    def stop_game(self):
        pass
