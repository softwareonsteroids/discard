class PlayerState(object):
    def __init__(self, game_state, cards_played, current_deck):
        self._player_state = game_state
        self.cards_played = cards_played
        self.current_deck = current_deck
        self.num_of_cards_played = 0 	# This records the number of cards that have been played
        self.is_blocking = False
        self._win_status = None

    def get_list_of_moves(self):
        return self.cards_played

    def get_last_cards_played(self):
        return self.cards_played[-self.num_of_cards_played:]

    @property
    def win_status(self):
        return self._win_status

    @win_status.setter
    def win_status(self, status):
        """One of the enums Gamestate"""
        self._win_status = status

    @property
    def player_state(self):
        return self._player_state

    @player_state.setter
    def player_state(self, state):
        self._player_state = state
