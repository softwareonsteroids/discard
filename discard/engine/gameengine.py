import logging
from discard.lib import (
    NormalCard,
    SpecialCard,
    SpecialCardName
)


class GameEngine(object):
    def __init__(self, controller):
        self.controller = controller
        self.logger = logging.getLogger(__name__)
        self.played_cards, self.state, self.playing = None, None, None
        self.num_of_pick_cards = 0
        self.num_of_cards_to_discard = 0
        self.is_there_a_winner = False

    '''BEGIN: Check methods'''
    def is_card_normal(self, card):
        return isinstance(card, NormalCard)

    def is_card_special(self, card):
        return isinstance(card, SpecialCard)

    def is_card_pickone(self, card):
        if self.is_card_special(card):
            return card.char == SpecialCardName.PICKONE.value

    def is_card_picktwo(self, card):
        if self.is_card_special(card):
            return card.char == SpecialCardName.PICKTWO.value

    def is_card_question(self, card):
        if self.is_card_special(card):
            return card.char == SpecialCardName.QUESTION.value

    def is_card_skip(self, card):
        if self.is_card_special(card):
            return card.char == SpecialCardName.SKIP.value

    def is_card_drop(self, card):
        if self.is_card_special(card):
            return card.char == SpecialCardName.DROP.value
    '''END: Check methods'''
