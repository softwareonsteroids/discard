import logging
import sys
import asyncio

from tornado import (
    web, options, ioloop, websocket, httpserver
)
from pathlib import Path
from discard.lib import (
    Message,
    Response,
    Request,
    Config
)
from discard.backend.roomserver import RoomServer
from discard.backend.gameserver import GameServer


def enable_server_logging():
    path = str(Path.cwd().joinpath('logs', 'mainserver.log'))
    options.options['log_file_prefix'] = path
    options.parse_command_line()

    root_logger = logging.getLogger()
    root_logger.setLevel(logging.DEBUG)

    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(logging.Formatter(logging.BASIC_FORMAT))

    root_logger.addHandler(stream_handler)


class RMHandler(web.RequestHandler):
    def initialize(self, controller, logger):
        self.room_server = controller
        self.logger = logger

    def write_error(self, status_code, **kwargs):
        err_cls, err, traceback = kwargs['exc_info']
        if isinstance(err, web.HTTPError) and err.log_message:
            msg_ = Message(cmd=Response.ERROR,
                              data=err.log_message)
            self.write(Message.to_json(msg_))

    def get(self):
        rep = self.get_query_argument('cmd')
        cmd = Request[rep]
        msg_ = {}
        if cmd == Request.GET_ROOMMATES:
            user_id = self.get_query_argument('user_id')
            room_id = self.get_query_argument('room_id')
            list_of_roomates = self.room_server.get_all_roomates(user_id, room_id)
            msg_ = Message(
                cmd=Response.GET_ROOMMATES,
                roomates=list_of_roomates
            )
        elif cmd == Request.GET_ROOMS:
            list_of_rooms = self.room_server.get_all_rooms()
            msg_ = Message(
                cmd=Response.GET_ROOMS,
                rooms=list_of_rooms
            )
        self.logger.debug('[RoomHandler] Sent back to the client:{0}'.format(str(msg_)))
        self.write(Message.to_json(msg_))

    def post(self):
        recv_data = Message.to_obj(self.request.body)
        self.logger.debug('[RoomHandler] Post Object received: {0}'.format(recv_data))
        user_id = recv_data.get_payload_value('user_id')
        cmd = recv_data.cmd
        msg_snd = {}
        if cmd == Request.CREATE_A_ROOM:
            user_name = recv_data.get_payload_value('user_name')
            num_of_players = recv_data.get_payload_value('num_of_players')
            room_name = recv_data.get_payload_value('room_name')
            room_id = self.room_server.add_room(num_of_players, room_name)
            self.room_server.add_player(room_id, user_id, user_name)
            msg_snd = Message(
                cmd=Response.CREATE_A_ROOM,
                room_id=room_id
            )
        elif cmd == Request.JOIN_ROOM:
            room_id = recv_data.get_payload_value('room_id')
            user_name = recv_data.get_payload_value('user_name')
            if self.room_server.can_join(room_id, user_id):
                self.room_server.add_player(room_id, user_id, user_name)
                self.logger.info('You have been added to room: {0}'.format(str(room_id)))
                self.logger.debug('[RoomHandler] You have been added to room: {0}'.format(str(room_id)))
                msg_snd = Message(
                    cmd=Response.JOIN_ROOM,
                    prompt='You have been added to room: {0}'.format(str(room_id))
                )
            else:
                raise web.HTTPError(status_code=500,
                                    log_message='Room is full')
        self.logger.debug('[RoomHandler] Sent back to the client:{0}'.format(str(msg_snd)))
        self.write(Message.to_json(msg_snd))


class WSHandler(websocket.WebSocketHandler):
    def initialize(self, controller, logger):
        # Called on each request to the handler
        self.room_server = controller
        self.logger = logger
        self.client_id = None

    def check_origin(self, origin):
        return True

    def open(self, *args, **kwargs):
        self.client_id = self.get_argument('user_id')
        room_id = self.get_argument('room_id')
        _ = self.room_server.add_game_conn(self.client_id, room_id, self)
        self.logger.debug(f'[GameHandler] Websocket opened. ClientID = {self._client_id}')

    def on_message(self, message):
        self.logger.debug('[GameHandler] received message {msg} from client'.format(msg=message))
        self.room_server.handle_msg_from_client(message, self.data)

    def on_pong(self, data):
        # Invoked when the response to a ping frame is received from the client
        # Refer to https://github.com/tornadoweb/tornado/issues/2532, and https://github.com/tornadoweb/tornado/issues/2021
        # since the on_message handler is priority, the on_pong handler is made asynchronous
        # so that the pong does not block and close connection
        asyncio.ensure_future(self.room_server.on_pong_async())

    def on_close(self):
        self.room_server.remove_game_conn(self.client_id)


class Server(web.Application):
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        # self._set_file_logger()
        self.room_server = RoomServer()

        handlers = [
            (
                rf'{Config.NETCLIENT_GAMESERVER_URL}',
                WSHandler,
                dict(controller=self.room_server, logger=self.logger)
            ),
            (
                rf'{Config.NETCLIENT_MAINSERVER_URL}',
                RMHandler,
                dict(controller=self.room_server, logger=self.logger)
            )
        ]
        web.Application.__init__(self, handlers, websocket_ping_interval=10)

    def close(self):
        if not self.room_server.user_exit:
            self.room_server.handle_msg_from_client(
                Message.to_json(
                    Message(
                        cmd=Request.GAME_REQUEST,
                        next_cmd=Request.SHUTDOWN_GAME_SERVER
                    )
                )
            )

# def make_app():
#     return web.Application([
#         (rf'{Config.NETCLIENT_GAMESERVER_URL}', WSHandler)
#         (rf'{Config.NETCLIENT_MAINSERVER_URL}', RMHandler)
#     ], websocket_ping_interval=10)


if __name__ == "__main__":
    enable_server_logging()
    app, game_server_thr = None, None
    try:
        game_server_thr = GameServer()
        game_server_thr.start()
        app = Server()
        server = httpserver.HTTPServer(app)
        server.listen(int(Config.MAINSERVER_PORT.value))
        ioloop.IOLoop.current().start()
    except (SystemExit, KeyboardInterrupt):
        app.close()
        game_server_thr.join()
        ioloop.IOLoop.current().stop()
        print('Stopped server')
