import sys
import logging
import zmq

from uuid import uuid4
from discard.lib import (
    GameStatus,
    Request,
    Response,
    MessageDestination,
    Message,
    Config
)
from discard.backend.room import Room


class RoomServer(object):
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.rooms = []
        self.user_exit = False
        self.context = zmq.Context.instance()
        self.game_socket = self.context.socket(zmq.PAIR)
        self.game_socket.connect(Config.ROOMSERVER_GAMESERVER_URL.value)
        self.game_socket.setsockopt(zmq.LINGER, 0)
        self.stream_sock = zmq.eventloop.zmqstream.ZMQStream(self.game_socket)
        self.stream_sock.on_recv(self.send_reply_from_gameserver)

    def add_room(self, num_of_players, room_name):
        room_id = uuid4().hex
        self.rooms.append(Room(room_name, num_of_players, room_id))
        return room_id

    def add_player(self, room_id, user_id, user_name):
        for room in self.rooms:
            if room.room_id == room_id:
                if room.can_join(user_id):
                    room.add_player(user_name, user_id)
                    self.logger.debug(f'Added player {user_name} to room {room_id}')
                    print(f'Added player {user_name} to room {room_id}')
                    return True
                else:
                    return False

    def can_join(self, room_id, user_id):
        for room in self.rooms:
            if room.room_id == room_id:
                return room.can_join(user_id)

    def get_all_rooms(self):
        return [
            dict(
                room_name=room.room_name,
                room_id=room.room_id,
                num_of_players_remaining=room.get_num_of_players_remaining()
            ) for room in self.roomss
        ]

    def get_all_roomates(self, user_id, room_id):
        roomates = []
        for room in self.rooms:
            if room.room_id == room_id:
                roomates = [
                    dict(
                        user_id=player.get('user_id'),
                        user_name=player.get('user_name')
                    ) for player in room.players if player.get('user_id') != user_id
                ]
                break
        return roomates

    def remove_game_conn(self, user_id):
        for room in self.rooms:
            for player in room.players:
                if player.get('user_id') == user_id:
                    username = player.get('user_name')
                    room.update_user(user_id, game_conn=None)
                    print(f"{username}'s connection has been dropped")
                    self.logger.debug(f"{username}'s connection has been dropped")
                    break

    def remove_all_game_conns(self):
        for room in self.rooms:
            for player in room.players:
                user_id = player.get('user_id')
                username = player.get('user_name')
                room.update_user(user_id, game_conn=None)
                print(f"{username}'s connection has been dropped")
                self.logger.debug(f"{username}'s connection has been dropped")

    def cleanup(self, send_to_game_server=False):
        self.logger.debug('Cleaning up')
        if send_to_game_server:
            self.game.socket.send_pyobj(
                Message(cmd=Request.SHUTDOWN_GAME_SERVER)
            )
        self.game_socket.close()
        self.context.term()

    def shutdown(self):
        return all([
            player.get('wbsocket') is None for room in self.rooms
            for player in room.players
        ])

    def send_reply_from_roomserver(self, user_id, room_id, msg, broadcast=False):
        for room in self.rooms:
            if room.room_id == room_id:
                if broadcast:
                    for player in room.players:
                        player['pong_data'] = Message.to_json(msg)
                else:
                    for player in room.players:
                        if player.get('user_id') == user_id:
                            player['pong_data'] = Message.to_json(msg)
                        else:
                            player['pong_data'] = None
                return

    def add_game_conn(self, user_id, room_id, game_conn):
        for room in self.rooms:
            if room.room_id == room_id:
                for player in room.players:
                    if player.get('user_id') == user_id:
                        room.update_user(user_id, game_conn=game_conn)
                        self.logger.debug(f'Added websocket conn for player {user_id}')
                        resp = Message(
                            cmd=Response.ADDED_NEW_GAME_CONN,
                            prompt='Added websocket conn for player {0}'.format(user_id)
                        )
                        self.send_reply_from_roomserver(
                            user_id,
                            room_id, resp, broadcast=True)

    def handle_msg_from_client(self, msg):
        msg_rcv = Message.to_obj(msg)
        self.logger.debug(f'Received message from client: {msg_rcv}')
        room_id = msg_rcv.get_payload_value('room_id')
        user_id = msg_rcv.get_payload_value('user_id')
        if msg_rcv.cmd == Request.START_GAME:
            for room in self.rooms:
                if room.room_id == room.id:
                    self.logger.debug(f'Room: {str(room)}')
                    if room.is_open():
                        self.logger.debug(f'Waiting for {str(room.get_num_of_players_remaining())} players to join')
                        response = Message(
                            cmd=Response.START_GAME,
                            prompt=f'Waiting for {str(room.get_num_of_players_remaining())} players to join'
                        )
                        self.send_reply_from_roomserver(user_id, room_id, response)
                    elif all([room.is_full(), room.has_game_not_started()]):
                        room.game_status = GameStatus.IS_STARTING
                        players = [player.get('user_id') for player in room.players]
                        self.game_socket.send_pyobj(Message(
                            cmd=Request.START_GAME,
                            players=players,
                            room_id=room_id,
                            delivery=MessageDestination.UNICAST
                        ))
                    response = Message(
                        cmd=Response.START_GAME,
                        prompt=Response.GAME_IS_STARTING
                    )
                    self.send_reply_from_roomserver(
                        user_id, room_id, response)
        elif msg_rcv.cmd == Request.GAME_REQUEST:
            self.game_socket.send_pyobj(Message(
                cmd=msg_rcv.get_payload_value('next_cmd'),
                **msg_rcv.get_payload_value('data')
            ))
            if msg_rcv.get_payload_value('next_cmd') == Request.SHUTDOWN_GAME_SERVER:
                self.cleanup()

    def send_reply_from_gameserver(self, msgs):
        msg_rcv = Message.to_obj(msgs[0].decode())
        self.logger.debug(f'Received msg from gameserver(Decoded): {str(msg_rcv)}')
        room_id = msg_rcv.get_payload_value('room_id')
        if msg_rcv.get_payload_value('cmd') != Response.STOP_GAME:
            for room in self.rooms:
                if room.room_id == room_id:
                    if msg_rcv.get_payload_value('delivery') == MessageDestination.UNICAST:
                        for player in room.players:
                            if player.get('user_id') == msg_rcv.get_payload_value('user_id'):
                                player['pong_data'] = Message.to_json(msg_rcv)
                                print(f"Publishing a GAME response to player: {msg_rcv.get_payload_value('user_id')}")
                                self.logger.debug(f"Publishing a GAME response to player: {msg_rcv.get_payload_value('user_id')}")
                                return
                    else:
                        for player in room.players:
                            player['pong_data'] = Message.to_json(msg_rcv)
                        print('Publishing a GAME response to player')
                        self.logger.debug('Publishing a GAME response to player')
                    return
        elif msg_rcv.get_payload_value('cmd') == Response.STOP_GAME:
            if msg_rcv.get_payload_value('delivery') == MessageDestination.BROADCAST:
                if not self.shutdown():
                    self.remove_all_game_conns()
                    self.cleanup()
                    self.user_exit = True
                    sys.exit(0)
            else:
                for user_id in msg_rcv.get_payload_value('players'):
                    self.remove_game_conn(user_id)
                if self.shutdown():
                    self.cleanup(True)
                    self.user_exit = True
                    sys.exit(0)

    async def on_pong_async(self):
        self.logger.debug('Websocket ping from client')
        for room in self.rooms:
            for player in room.players:
                ws = player.get('wbsocket')
                if ws:
                    if not player.get('pong_data'):
                        await ws.write_message(Message.to_json(
                            Message(cmd=Response.GAME_PONG)
                        ))
                    else:
                        await ws.write_message(player.get('pong_data'))
                        player['pong_data'] = None
