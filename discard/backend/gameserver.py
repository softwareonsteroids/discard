import zmq
import logging
from uuid import uuid4
from discard.lib import (
    Message,
    Request,
    Response,
    GameStatus,
    Config,
    MessageDestination
)
from discard.engine import Controller
from threading import Thread


class GameServer(Thread):
    def __init__(self):
        super(GameServer, self).__init__()
        self.logger = logging.getLogger(__name__)
        self.ctx = zmq.Context.instance()
        self.socket = self.ctx.socket(zmq.PAIR)
        self.socket.setsockopt(zmq.LINGER, 0)
        self.socket.bind(Config.ROOMSERVER_GAMESERVER_URL.value)
        self.games = []
        self.daemon = True
        self.user_exit = False

    def create_game(self, msg):
        game_id = uuid4().hex
        g = Controller(msg.get_payload_value('players'))
        self.games.append(dict(
            game_id=game_id,
            game=g,
            game_status=GameStatus.STARTED
        ))
        for player in msg.get_payload_value('players'):
            self.socket.send_string(
                Message.to_json(
                    Message(
                        cmd=Response.START_GAME,
                        prompt=Response.GAME_HAS_STARTED,
                        cards=g.cards_for_player(player),
                        user_id=player,
                        game_id=game_id,
                        extra_data=g.top_card(),
                        room_id=msg.get_payload_value('room_id'),
                        delivery=msg.get_payload_value('delivery')
                    )
                )
            )

    def find_game(self, msg):
        index = next((
            index for index, game in enumerate(self.games)
            if game.get('game_id') == msg.get_payload_value('game_id'))
            , None)
        item = self.games[index]
        game = item.get('game')
        return item, game, index

    def set_initial_player(self, msg):
        item, game, index = self.find_game(msg)
        current_player = game.set_initial_player(
            msg.get_payload_value('user_id')
        )
        item['game'] = game
        self.games[index] = item
        if not current_player:
            self.socket.send_string(
                Message.to_json(
                    Message(
                        cmd=Response.SET_FIRST_PLAYER,
                        prompt=f"{msg.get_payload_value('user_name')} is now the initial player",
                        user_id=msg.get_payload_value('user_id'),
                        room_id=msg.get_payload_value('room_id'),
                        delivery=msg.get_payload_value('delivery')
                    )
                )
            )
            print(f"{msg.get_payload_value('user_name')} is now the initial player")
        else:
            self.socket.send_string(
                Message.to_json(
                    Message(
                        cmd=Response.SET_FIRST_PLAYER,
                        prompt=f"{msg.get_payload_value('user_name')} is already the initial player",
                        user_id=current_player,
                        room_id=msg.get_payload_value('room_id'),
                        delivery=msg.get_payload_value('delivery')
                    )
                )
            )
            print(f"{msg.get_payload_value('user_name')} is already the initial player")

    def get_game_status(self, msg):
        item, game, _ = self.find_game(msg)
        current_player = game.current_player
        self.socket.send_string(
            Message.to_json(
                Message(
                    cmd=Response.GET_GAME_STATUS,
                    user_id=current_player,
                    game_status=item.get('game_status'),
                    room_id=msg.get_payload_value('room_id'),
                    delivery=msg.get_payload_value('delivery')
                )
            )
        )

    def play_move(self, msg):
        pass

    def cleanup(self):
        self.socket.close()
        self.ctx.term()
        self.logger.debug(f'Closing game server')

    def close(self, msg):

        if msg.get_payload_value('delivery') == MessageDestination.UNICAST:
            item, game, index = self.find_game(msg)
            if item['game_status'] != GameStatus.ENDED:
                item['game_status'] = GameStatus.ENDED
                game.close_game()
                item['game'] = game
                self.games[index] = item
                self.socket.send_string(
                    Message.to_json(
                        Message(
                            cmd=Response.STOP_GAME,
                            users=msg.get_payload_value('players'),
                            delivery=msg.get_payload_value('delivery')
                        )
                    )
                )
        elif msg.get_payload_value('delivery') == MessageDestination.BROADCAST:
            if not self.user_exit:
                self.user_exit = True
                self.socket.send_string(
                    Message.to_json(
                        Message(
                            cmd=Response.STOP_GAME,
                            delivery=msg.get_payload_value('delivery')
                        )
                    )
                )
                self.logger.debug('Sent STOP_GAME to socket')
                self.cleanup()

    def run(self):
        self.logger.debug('Started Game Server')
        try:
            while not self.user_exit:
                msg_recv = self.socket.recv_pyobj()
                self.logger.debug(f'Message received: {msg_recv}')
                if msg_recv.cmd == Request.START_GAME:
                    self.create_game(msg_recv)
                elif msg_recv.cmd == Request.SET_INITIAL_PLAYER:
                    self.set_initial_player(msg_recv)
                elif msg_recv.cmd == Request.GET_GAME_STATUS:
                    self.get_game_status(msg_recv)
                elif msg_recv.cmd == Request.PAUSE_GAME:
                    pass
                elif msg_recv.cmd == Request.PLAY_MOVE:
                    pass
                elif msg_recv.cmd == Request.STOP_GAME:
                    self.close(msg_recv)
                elif msg_recv.cmd == Request.SHUTDOWN_GAME_SERVER:
                    self.cleanup()
        except zmq.ZMQError as e:
            # when the roomserver closes its socket before the gameserver has a
            # chance to do so
            self.logger.error(f'Error: {e}')
            if not self.user_exit:
                self.user_exit = True
                self.cleanup()
        except KeyboardInterrupt:
            if not self.user_exit:
                self.logger.debug('Closing from GAMESERVER')
                self.close(Message(
                    cmd=Request.STOP_GAME,
                    delivery=MessageDestination.BROADCAST
                ))
